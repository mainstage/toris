## v0.1.0 (2023-04-15)

## v0.1.0a11 (2023-04-10)

### Fix

- option does not accept "--" as name, allows arg forwarding
- Handle parsable.help is None in HelpFormatter
- Add missing format string prefix in error msg

## v0.1.0a10 (2023-03-30)

### Feat

- Allow preventing exception re-raise in hook_exit

### Fix

- Handle multiline strings in opt/args help

## v0.1.0a9 (2023-03-09)

### Fix

- Handle missing nargs=+ argument

## v0.1.0a8 (2023-03-09)

### Feat

- Add dest kwarg to parsables

## v0.1.0a7 (2023-03-06)

### Feat

- Allow customizing Clis rich theme
- Expose AppExit exception in main module

## v0.1.0a6 (2023-03-05)

### Fix

- Fix a small typo in group.py

## v0.1.0a5 (2023-03-04)

### Feat

- Allow to make Cli classes based on your own Group class

## v0.1.0a4 (2023-03-04)

### Feat

- Add Group.log helper

### Fix

- Remove Cli.help default value. Run cleandoc on group.help

## v0.1.0a3 (2023-03-03)

### Feat

- Rename Main to Cli, add hide command option

## v0.1.0a2 (2023-03-03)

### Feat

- Nicer error message on unknow argument

### Fix

- List subgroups in command help

## v0.1.0a1 (2023-03-03)

### Fix

- cz bump version in pyprojet.toml and __version__.py

## v0.1.0a0 (2023-03-03)

### Feat

- Help formatter, hooks, rename project
- Initial commit, complete parser, basic commands

### Fix

- test_command name conflict
