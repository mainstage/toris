import inspect

from typing import Optional


def command(
    parse=[],
    label: Optional[str] = None,
    summary: Optional[str] = None,
    help: Optional[str] = None,
    hide: bool = False,
):
    """
    A decorator marking the method as an executable CLI command
    """

    def decorator(func):
        meta = {"parse": parse, "summary": summary, "hide": hide}

        if label is None:
            meta["label"] = func.__name__
        else:
            meta["label"] = label

        if help is not None:
            meta["help"] = help
        elif func.__doc__ is not None:
            meta["help"] = inspect.cleandoc(func.__doc__)
        else:
            meta["help"] = f"{meta['label']} command"

        func.__toris_meta__ = meta

        return func

    return decorator
