import inspect, logging

from typing import Optional, List
from rich import print

from .parsable import Parsable, CommandChoice
from .errors import HelpRequestedError, AppExit

from .help import HelpPrinter


class Group:
    """
    A group of Commands
    """

    label: str
    help: Optional[str] = None
    summary: Optional[str] = None
    parse: List[Parsable] = []
    subgroups: List["Group"] = []

    @classmethod
    @property
    def _group_help(cls):
        if cls.help is not None:
            return inspect.cleandoc(cls.help)
        if cls.__doc__:
            return inspect.cleandoc(cls.__doc__)
        return ""

    def __init__(self, parent=None):
        self._parent = parent
        self._hook_init()

    @property
    def stdout(self):
        "Returns the standard output rich.Console"
        return self.main.stdout

    @property
    def stderr(self):
        "Returns the standard error rich.Console"
        return self.main.stderr

    def print(self, *args, **kwargs):
        """
        Convenience wrapper to call the rich.print method on the stdout
        console object.
        """
        return self.stdout.print(*args, **kwargs)

    def exit(self, code=0):
        """
        Request immediate termination of the app with the provided return code
        """
        raise AppExit(code)

    def _hook_init(self):
        self.hook_init()

    def hook_init(self):
        """
        Hook called just after object creation
        """
        pass

    def _hook_pre_parse(self, parser):
        self.hook_pre_parse(parser)

    def hook_pre_parse(self, parser):
        """
        Hook called before argument parsing.
        """
        pass

    def _run_hook_post_parse(self):
        if self._parent is not None:
            self._parent._run_hook_post_parse()
        self._hook_post_parse()

    def _hook_post_parse(self):
        self.hook_post_parse()

    def hook_post_parse(self):
        """
        Hook called after parsing has been done, but before command
        execution. `self.args` is available
        """
        pass

    def _hook_exit(self, exception=None) -> bool:
        return self.hook_exit(exception)

    def hook_exit(self, exception=None) -> bool:
        """
        Hook called after command execution.

        If exception is not None, the command raised an exception. It can
        be an actual error or AppExit/HelpRequestedError

        :return: bool. If exception is not None, the exception will be re-raise
            and bubble up the stack unless you return True here
        """
        pass

    @property
    def main(self):
        "Accessor to the main Cli object"
        if not hasattr(self, "__main"):
            self.__main = self.__find_main()

        return self.__main

    @property
    def args(self):
        "Returns the parsed args as a namespace"
        return self.main.args

    @property
    def app(self):
        """
        A convenience helper to return the `app` property from the main CLI.
        You can override the `app` property of the main object to have a
        global context.
        """
        return self.main.app

    @property
    def nesting_hierarchy(self) -> List["Group"]:
        """
        Return the list of Group objects instantiated during parsing leading
        from the main Cli object to the current group
        """
        if self._parent is None:
            return [self]
        return self._parent.nesting_hierarchy + [self]

    @property
    def log_name(self):
        """
        Return the name to use in the logging hierarchy. Returns the label by
        default
        """
        return self.__class__.label

    @property
    def log(self) -> logging.Logger:
        """
        Returns a logger for the current group
        """
        log_path = [group.log_name for group in self.nesting_hierarchy]
        return logging.getLogger(".".join(log_path))

    def __find_main(self):
        parent = self

        while parent._parent is not None:
            parent = parent._parent

        return parent

    def _add_parsables(self, parser, parsables, default_group=None):
        for parsable in parsables:
            if parsable.group is None:
                parsable.group = default_group

        parser.add(*parsables)

    def _collect_commands(self):
        commands = []

        for member_name in dir(self.__class__):
            member = getattr(self.__class__, member_name)
            if hasattr(member, "__toris_meta__"):
                commands.append(member)

        return commands

    def _get_command(self, name):
        for cmd in self._collect_commands():
            if cmd.__toris_meta__["label"] == name:
                return cmd
        raise RuntimeError(f"{self.__class__}: no command with name {name}")

    def _dispatch_to_command(self, parser, name):
        cmd = self._get_command(name)
        parsables = cmd.__toris_meta__["parse"]

        self._add_parsables(parser, parsables, cmd.__toris_meta__["label"])

        try:
            parser.parse()
        except HelpRequestedError:
            HelpPrinter.help(self, parser, cmd)
            self.exit()

        # We're about to run a command. Let's push back the parsing result
        # to the main object
        self.main.args = parser.results

        # Invoking the post_parse hook in descending order
        self._run_hook_post_parse()

        # Actually running the command
        cmd(self)

    def _find_subgroup(self, name):
        for group in self.subgroups:
            if group.label == name:
                return group

        raise RuntimeError(f"{self.__class__}: no subgroup with name {name}")

    def _dispatch_to_subgroup(self, parser, name):
        group_klass = self._find_subgroup(name)
        group = group_klass(self)

        group.do_parse(parser)

    def do_parse(self, parser):
        try:
            try:
                self.do_parse_inner(parser)
            except HelpRequestedError:
                HelpPrinter.help(self, parser)
                self.exit()
        except Exception as exception:
            prevent_reraise = self._hook_exit(exception)
            if not prevent_reraise:
                raise exception

    def do_parse_inner(self, parser):
        self._add_parsables(parser, self.parse, self.label)

        commands = [c.__toris_meta__["label"] for c in self._collect_commands()]
        subgroups = [group.label for group in self.subgroups]

        if len(commands + subgroups) == 0:
            raise RuntimeError(f"{self.__class__} has no commands or subgroups")

        cmd_token = CommandChoice(*(commands + subgroups))
        parser.add(cmd_token)

        self._hook_pre_parse(parser)
        res = parser.parse()

        if isinstance(res, CommandChoice):
            if res.value in commands:
                self._dispatch_to_command(parser, res.value)
            else:
                self._dispatch_to_subgroup(parser, res.value)
            self._hook_exit(None)
        else:
            raise HelpRequestedError()
