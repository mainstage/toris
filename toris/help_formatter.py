import inspect

from typing import Optional, List, Union

from rich import box, print
from rich.markdown import Markdown
from rich.padding import Padding
from rich.panel import Panel
from rich.table import Table
from rich.text import Text
from rich.pretty import Pretty
from rich.align import Align

from .parser import CommandChoice
from .arguments import Argument, Flag


HELP_COMMANDS = "Commands"
HELP_USAGE = "Usage"
HELP_USAGE_COMMAND = "[usage_group]COMMAND[/]"
HELP_USAGE_OPTS = "[usage_ops]\[OPTIONS][/]"
HELP_BOX = box.ASCII


class DefaultHelpFormatter:
    def __init__(self, group, console):
        self.group = group
        self.console = console

    def print(self, *args, **kwargs):
        return self.console.print(*args, **kwargs)

    def print_prolog(self):
        pass

    def print_epilog(self):
        pass

    def print_usage(
        self,
        main_name: str,
        current_pos: List[Union[str, Argument, CommandChoice]],
        future_pos: List[Union[str, Argument, CommandChoice]],
        help: Optional[str],
        is_cmd: bool,
    ):
        # Usage: main_name
        usage = f"[usage]{HELP_USAGE}:[/usage]"
        usage += f" [usage_name]{main_name}[/usage_name]"

        # Add previously provided groups and arguments
        current_pos = self._format_positionals(current_pos)
        usage += f" {current_pos}"

        # Add [OPTIONS]
        usage += f" {HELP_USAGE_OPTS}"

        # Add not provided positionals
        future_pos = self._format_positionals(future_pos)
        usage += f" {future_pos}"

        self.print(Padding(f"[usage_line]{usage}[/usage_line]", 1))
        if help:
            self.print(Padding(Markdown(help), (0, 0, 1, 2)))

    def print_subcommands_list(self, groups, metas):
        table = self._make_table()
        table.add_column(
            "Command", justify="left", style="cyan", no_wrap=True, min_width=10
        )
        table.add_column("Summary", justify="left")

        for group in groups:
            summary = group.summary or group._group_help or ""
            if summary.splitlines():
                summary = summary.splitlines()[0]

            table.add_row(group.label, summary)

        for meta in metas:
            if not meta["hide"]:
                table.add_row(meta["label"], meta["summary"] or "")

        self.print(self._make_panel(table, title="Commands"))

    def print_arguments(self, args):
        table = self._make_table()
        table.add_column(
            "Argument", justify="left", style="cyan", no_wrap=True, min_width=10
        )
        table.add_column("Count", style="dim", justify="left")
        table.add_column("Help", justify="left")

        for arg in args:
            table.add_row(
                arg.metavar.upper(),
                self._format_arg_nargs(arg),
                inspect.cleandoc(arg.help or "").replace("\n", " "),
            )

        self.print(self._make_panel(table, title="Arguments"))

    def print_options(self, opts):
        table = self._make_table()
        table.add_column(
            "Long Option", justify="left", style="cyan", min_width=10
        )
        table.add_column(
            "Short Option",
            justify="left",
            style="cyan dim",
        )
        table.add_column("Type", style="yellow", justify="left")
        table.add_column("Help", justify="left")

        for opt in opts:
            table.add_row(
                self._format_opt_long(opt),
                ", ".join(opt.short),
                self._format_opt_type(opt),
                self._format_opt_help(opt),
            )

        self.print(self._make_panel(table, title="Options"))

    def _make_table(self, **kw):
        return Table(
            show_header=False,
            show_edge=False,
            show_lines=False,
            highlight=False,
            pad_edge=False,
            box="",
            **kw,
        )

    def _make_panel(self, content, **kw):
        return Padding(
            Panel(
                content,
                title_align="left",
                border_style="bright_black",
                box=HELP_BOX,
                **kw,
            ),
            (0, 0, 0, 0),
        )

    def _format_opt_long(self, opt):
        names = []

        if not isinstance(opt, Flag):
            names = [f"{name}[dim]=[/dim]" for name in opt.long]
        else:
            names = opt.long

        return ", ".join(names)

    def _format_opt_type(self, opt):
        if isinstance(opt, Flag):
            return ""

        if opt.type is None:
            result = "str"
        else:
            result = opt.type.__name__

        return result

    def _format_opt_help(self, opt):
        tags = []

        if opt.deprecated:
            tags.append(f"[opt_deprecated]deprecated[/]")
        if opt.required:
            tags.append("[opt_required]required[/]")
        if opt.default:
            tags.append(f"[opt_default]default: {opt.default}[/]")
        if opt.action in ["append", "append_const", "count"]:
            tags.append(f"[opt_repeat]repeat[/]")
        tags = ", ".join(tags)
        if tags:
            tags = f"[opt_tags]\[{tags}][/]"

        result = []
        if opt.help:
            result.append(inspect.cleandoc(opt.help).replace("\n", " "))
        if len(tags) > 0:
            result.append(tags)

        return " ".join(result)

    def _format_arg_metavar(self, arg):
        metavar = arg.metavar.upper()
        if isinstance(arg.nargs, int):
            return " ".join([metavar] * arg.nargs)
        if arg.nargs == "?":
            return f"[{metavar}]"
        if arg.nargs == "+":
            return f"{metavar} [...]"
        if arg.nargs == "*":
            return f"[{metavar} [...]]"

    def _format_arg_nargs(self, arg):
        if isinstance(arg.nargs, int):
            return f"{arg.nargs}"
        if arg.nargs == "?":
            return "?"
        if arg.nargs == "+":
            return "+"
        if arg.nargs == "*":
            return "*"

    def _format_positionals(self, positionals):
        result = []

        for item in positionals:
            if isinstance(item, CommandChoice):
                value = item.value
                if value is None:
                    result.append(HELP_USAGE_COMMAND)
                else:
                    result.append(f"[usage_group]{value}[/]")
            elif isinstance(item, Argument):
                formatted_metavar = self._format_arg_metavar(item)
                result.append(f"[usage_args]{formatted_metavar}[/]")

        return " ".join(result)
