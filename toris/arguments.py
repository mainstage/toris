import re

from typing import Union

from .errors import (
    ValueNeededError,
    NotEnoughArgumentsError,
)

from .parsable import Parsable


class Option(Parsable):
    """
    An option with an input, like `--item=value` or `-v 3`
    """

    parser_prepend = False

    def __init__(self, *names, **kw):
        if "metavar" not in kw:
            kw["metavar"] = self.__class__._choose_metavar(names)

        super().__init__(**kw)

        # Short form parsable tokens, like '-h'
        self.short = []
        # Long form parsable tokens, like '--help'
        self.long = []
        for name in names:
            if name.startswith("--"):
                self.long.append(name)
            elif name.startswith("-"):
                self.short.append(name)
            else:
                raise ValueError("Option name must start with `-` or `--`")

    @property
    def display_name(self):
        names = self.long + self.short
        first = names.pop(0)

        result = first
        if len(names) > 0:
            result += " (or {', '.join(names)})"

        return result

    @classmethod
    def _choose_metavar(cls, names):
        for name in names:
            if name.startswith("--"):
                return name.removeprefix("--").replace("-", "_")
        return names[0].removeprefix("-")

    def accept(self, parser):
        if not parser.top.startswith("-") or parser.top == "--":
            return False

        if parser.top.startswith("--"):
            return self.parse_long(parser)
        else:
            return self.parse_short(parser)

    def consume_long(self, parser, name):
        consumed = parser.pop()
        consumed = consumed.removeprefix(name)

        if consumed.startswith("="):
            self.value = consumed.removeprefix("=")
            return True
        elif parser.top is not None:
            self.value = parser.pop()
            return True
        else:
            raise ValueNeededError(self)

    def parse_long(self, parser):
        for name in self.long:
            if parser.top == name or parser.top.startswith(name + "="):
                return self.consume_long(parser, name)

    def consume_short(self, parser, name):
        parser.pop()

        if parser.top is not None:
            self.value = parser.pop()
            return True
        else:
            raise ValueNeededError(self)

    def parse_short(self, parser):
        for name in self.short:
            if parser.top == name:
                return self.consume_short(parser, name)

    def __rich_repr__(self):
        yield "names", self.long + self.short
        yield from super().__rich_repr__()


class Flag(Option):
    """
    A flag, or option without parameters like `-f` or `--enable-something`
    """

    def parse_long(self, parser):
        for name in self.long:
            if parser.top == name:
                return self.consume_long(parser, name)

    def consume_long(self, parser, name):
        parser.pop()
        self.value = self.default

        return True

    def parse_short(self, parser):
        for name in self.short:
            if parser.top.startswith(name):
                return self.consume_short(parser, name)

    def consume_short(self, parser, name):
        self.value = self.default
        if parser.top == name:
            parser.pop()
            return True
        else:
            parser.argv[0] = parser.argv[0].replace(name, "-")
            return True


class Argument(Parsable):
    """
    A positional argument
    """

    parser_remove = True

    def __init__(
        self,
        name,
        action=None,
        choices=None,
        const=None,
        default=None,
        dest=None,
        group=None,
        metavar=None,
        type=None,
        nargs: Union[int, str] = 1,
        help=None,
    ):
        self.name = name
        if metavar is None:
            metavar = name.replace("-", "_")

        if isinstance(nargs, str) and re.fullmatch(r"\d+", nargs):
            nargs = int(nargs)

        multiple = False
        if isinstance(nargs, int):
            required = True
            multiple = nargs > 1
        elif nargs == "+":
            required = True
            self.parser_terminal = True
            multiple = True
        elif nargs == "*":
            required = False
            self.parser_terminal = True
            multiple = True
        elif nargs == "?":
            required = False
        else:
            raise ValueError(f"Unknown value for nargs: '{nargs}'")
        self.nargs = nargs

        if multiple and default is None:
            default = []
        if action is None:
            if multiple:
                action = "append"
            else:
                action = "store"

        super().__init__(
            metavar=metavar,
            action=action,
            choices=choices,
            const=const,
            default=default,
            dest=dest,
            group=group,
            required=required,
            help=help,
            type=type,
        )

    def accept(self, parser):
        if isinstance(self.nargs, int):
            if len(parser.argv) >= self.nargs:
                for _ in range(self.nargs):
                    self.value = parser.pop()
                return True
            else:
                raise NotEnoughArgumentsError(
                    self, self.nargs, len(parser.argv)
                )
        elif self.nargs == "?":
            self.value = parser.pop()
            return True
        elif self.nargs in ["+", "*"]:
            for _ in range(len(parser.argv)):
                self.value = parser.pop()
            return True
        else:
            raise RuntimeError("How can this happen?")

    @property
    def display_name(self):
        return self.name

    def __rich_repr__(self):
        yield from super().__rich_repr__()
        yield "nargs", self.nargs


#
# Style alias for the module's methods
#
option = Option
o = Option
flag = Flag
f = Flag
argument = Argument
a = Argument
