import sys, shlex

from typing import Optional

from rich.console import Console
from rich.padding import Padding
from rich.theme import Theme

from .errors import AppExit, UnknownArgumentError
from .help import HelpFlag, HelpPrinter
from .help_formatter import DefaultHelpFormatter
from .parser import Parser
from .group import Group
from .style import rich_theme


def makeCli(group_class):
    if group_class is not Group and Group not in group_class.__bases__:
        raise ValueError("makeCli parameter must be a child of the Group class")

    class Cli(group_class):
        label: Optional[str] = None
        help_formatter = DefaultHelpFormatter

        theme: Theme = rich_theme
        exit_when_done: bool = True

        def __init__(self, argv):
            super().__init__()

            if isinstance(argv, str):
                self._argv = shlex.split(argv)
            else:
                self._argv = list(argv)

            if self._argv[0] == sys.argv[0]:
                invocation_name = self._argv.pop(0)
                if self.__class__.label is None:
                    self.label = invocation_name

        def _add_parsables(self, parser, parsables, _):
            super()._add_parsables(parser, parsables, default_group="global")

        @property
        def args(self):
            if hasattr(self, "_args"):
                return self._args
            return None

        @property
        def app(self):
            return None

        @args.setter
        def args(self, value):
            if hasattr(self, "_args"):
                raise AttributeError("Arguments have already been set on Main")
            self._args = value

        @property
        def log_name(self):
            return "main"

        @property
        def stdout(self):
            if not hasattr(self, "_stdout"):
                self._stdout = self.make_console()
            return self._stdout

        @property
        def stderr(self):
            if not hasattr(self, "_stderr"):
                self._stderr = self.make_console(stderr=True)
            return self._stderr

        def make_console(self, **kw):
            return Console(theme=self.__class__.theme, **kw)

        @classmethod
        def run(cls, argv=sys.argv, *args, **kwargs):
            cls(argv, *args, **kwargs)._run()

        def _run(self):
            parser = Parser(self._argv)
            parser.add(HelpFlag())

            try:
                self.do_parse(parser)
                if self.__class__.exit_when_done:
                    sys.exit(0)
            except AppExit as ex:
                if self.__class__.exit_when_done:
                    sys.exit(ex.code)
                else:
                    raise ex
            except UnknownArgumentError as ex:
                self.stderr.print(
                    Padding(f"Unknown argument: [italic]{ex.text}[/]", (1, 0))
                )
                self.stderr.print(
                    "[dim]Use [cyan]--help[/] to see "
                    "available arguments, commands and options."
                )

    return Cli


Cli = makeCli(Group)
