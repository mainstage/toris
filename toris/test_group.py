import logging

from . import Cli, Group, command


def test_logging_hier():
    loggers = []

    class Group2(Group):
        label = "g2"

        @command()
        def cmd(self):
            assert isinstance(self.log, logging.Logger)
            loggers.append(self.log.name)

    class Group1(Group):
        label = "g1"
        subgroups = [Group2]

    class Main(Cli):
        subgroups = [Group1]

    Main.run("g1 g2 cmd")

    assert loggers == ["main.g1.g2"]


def test_app_property():
    class Group2(Group):
        label = "g2"

        @command()
        def cmd(self):
            assert self.app == "app"

    class Group1(Group):
        label = "g1"
        subgroups = [Group2]

    class Main(Cli):
        subgroups = [Group1]

        @property
        def app(self):
            return "app"

    Main.run("g1 g2 cmd")
