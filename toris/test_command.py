import pytest

from .cli import Cli
from .group import Group
from .command import command
from .arguments import option, flag
from .errors import AppExit

calls = []


@pytest.fixture
def reset_calls():
    calls.clear()


class SubTest1(Group):
    __test__ = False

    label = "subtest1"
    parse = [flag("--subtest1")]

    @command()
    def cmd1(self):
        calls.append("cmd1")

    @command()
    def cmd2(self):
        calls.append("cmd2")


class TestCli1(Cli):
    __test__ = False

    label = "testmain1"
    subgroups = [SubTest1]
    parse = [option("--option1"), option("--option2"), flag("--flag1")]

    @command()
    def main_cmd(self):
        calls.append("main_cmd")


def test_basic_routing(reset_calls):
    TestCli1.run("main_cmd")

    assert calls == ["main_cmd"]


def test_basic_routing_subcmd(reset_calls):
    TestCli1.run("subtest1 --flag1 cmd1")

    assert calls == ["cmd1"]


def test_flag_after_cmd():
    class Sub(Group):
        label = "sub"
        parse = [flag("--flag", action="store_true")]
        called = False

        @command()
        def cmd(self):
            self.__class__.called = True
            assert self.args.flag

    class MyCli(Cli):
        subgroups = [Sub]

    MyCli.run("sub cmd --flag")
    assert Sub.called


def test_hooks_simple():
    hooks = []

    class Sub(Group):
        label = "sub"

        @command()
        def cmd(self):
            pass

        @command()
        def cmd_exit(self):
            self.exit()

        def hook_init(self):
            hooks.append("sub init")

        def hook_pre_parse(self, _):
            hooks.append("sub pre_parse")

        def hook_post_parse(self):
            hooks.append("sub post_parse")

        def hook_exit(self, _):
            hooks.append("sub exit")

    class MyCli(Cli):
        subgroups = [Sub]

        def hook_init(self):
            hooks.append("main init")

        def hook_pre_parse(self, _):
            hooks.append("main pre_parse")

        def hook_post_parse(self):
            hooks.append("main post_parse")

        def hook_exit(self, _):
            hooks.append("main exit")

    expected_hooks = [
        "main init",
        "main pre_parse",
        "sub init",
        "sub pre_parse",
        "main post_parse",
        "sub post_parse",
        "sub exit",
        "main exit",
    ]

    MyCli.run("sub cmd")
    assert hooks == expected_hooks

    hooks.clear()
    with pytest.raises(AppExit):
        MyCli.run("sub cmd_exit")
        assert hooks == expected_hooks

    hooks.clear()
    with pytest.raises(AppExit):
        MyCli.run("sub cmd -h")
        assert hooks == expected_hooks
