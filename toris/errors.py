#
# Parsing related Exceptions
#


class ParserError(ValueError):
    def __init__(self, msg, parsable=None):
        self.parsable = parsable
        super().__init__(msg)


class UnknownArgumentError(ParserError):
    def __init__(self, text):
        self.text = text
        super().__init__(f"Unknown argument/option {text}")


class InvalidChoiceError(ParserError):
    def __init__(self, parsable, choices, value):
        self.choices = choices
        self.value = value

        msg = f"{value} is not a valid choice for {parsable.display_name}. "
        msg += f"Allowed choices are {choices!r}"
        super().__init__(msg=msg, parsable=parsable)


class NotEnoughArgumentsError(ParserError):
    def __init__(self, parsable, needed, available):
        self.needed = needed
        self.available = available

        msg = f"{parsable.display_name}: {needed} arguments required, only "
        msg += f"{available} provided"
        super().__init__(msg=msg, parsable=parsable)


class ValueNeededError(ParserError):
    def __init__(self, parsable):
        super().__init__(
            msg=f"{parsable.display_name} needs a value",
            parsable=parsable,
        )


class RequiredArgumentError(ParserError):
    def __init__(self, parsable):
        super().__init__(
            msg=f"{parsable.display_name} is required", parsable=parsable
        )


class HelpRequestedError(ParserError):
    """
    Not strictly an error, but we use the exception to trigger the help
    from anywhere in the cli arguments
    """

    def __init__(self):
        super().__init__("Help requested")


#
# Command Execution Exceptions
#


class AppError(RuntimeError):
    pass


class AppExit(AppError):
    def __init__(self, code=0):
        self.code = code
        super().__init__("User requested exit")
