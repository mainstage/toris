from rich.theme import Theme

TORIS_STYLES = {
    # "usage": "yellow",
    "usage": "yellow bold",
    "usage_line": "",
    "usage_name": "italic bold",
    "usage_group": "bold",
    "usage_args": "not bold",
    "usage_opts": "dim",
    "opt_tags": "dim",
    "opt_deprecated": "magenta italic",
    "opt_required": "cyan",
    "opt_default": "",
    "opt_repeat": "italic",
    "ellipsis": "",
}

rich_theme = Theme(TORIS_STYLES)
