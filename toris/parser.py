from types import SimpleNamespace
from typing import Optional, Union

from rich import print

from .errors import ParserError, UnknownArgumentError, RequiredArgumentError
from .parsable import Parsable, CommandChoice
from .arguments import Argument, Option


class Parser:
    def __init__(self, argv, parsables=[]):
        self.argv = argv
        self.parsables = []
        self.parsed = []

        self.add(*parsables)

    def _append_parsable(self, parsable):
        if self.last_parsable and self.last_parsable.parser_terminal:
            raise ParserError(
                "Cannot add {parsable}. Last parsable is already terminal",
                parsable,
            )
        self.parsables.append(parsable)

    def add(self, *parsables):
        for parsable in parsables:
            if parsable.parser_prepend:
                self.parsables.insert(0, parsable)
            else:
                self._append_parsable(parsable)

    def _filter_args(self, parsables):
        arguments = []

        for item in parsables:
            if isinstance(item, Argument):
                arguments.append(item)

        return arguments

    @property
    def arguments(self):
        return self._filter_args(self.parsables)

    @property
    def parsed_arguments(self):
        return self._filter_args(self.parsed)

    @property
    def options(self):
        options = []

        for item in self.parsables:
            if isinstance(item, Option):
                options.append(item)

        return options

    def parse(self) -> Optional[Union["Parsable", bool]]:
        parsed_something = False

        # import pdb; pdb.set_trace()

        # Try to progress as long as we haven't consumed all input
        while len(self.argv) > 0:
            parsed = self.parse_next()

            # Debug print
            # print(f"--> top = {self.top}, parsed = ", parsed)

            # No Parsable accepted the top argv item. This is a wrong param
            if parsed is None:
                raise UnknownArgumentError(self.top)

            # We parsed something
            else:
                parsed_something = parsed_something or True
                # If this is a CommandChoice, we need to stop parsing to add
                # the CommandChoice Parsables
                if isinstance(parsed, CommandChoice):
                    return parsed

        # No more input to consume
        if parsed_something:
            return self.last_parsed
        return True

    def parse_next(self):
        """
        Iterates over all the Parsables, stop when one accepts the top input.

        :return: the parsable that accepted input or None
        """
        for parsable in self.parsables:
            if parsable.accept(self):
                self.parsed.append(parsable)
                if parsable.parser_remove:
                    self.parsables.remove(parsable)
                return parsable

    @property
    def last_parsed(self):
        if len(self.parsed) > 0:
            return self.parsed[0]

    @property
    def last_parsable(self):
        if len(self.parsables) > 0:
            return self.parsables[0]

    @property
    def top(self):
        if len(self.argv) > 0:
            return self.argv[0]

    def pop(self):
        if len(self.argv) > 0:
            return self.argv.pop(0)

    @property
    def results(self):
        results = {"_cmd_path_": []}

        for el in self.parsables + self.parsed:
            if el.metavar == "_cmd_":
                # CommandChoice can be in parsables but not yet parsed
                # if there was a `-h` before
                if el.value is not None:
                    results["_cmd_path_"].append(el.value)
            else:
                if el.required:
                    if el.value is None:
                        raise RequiredArgumentError(el)
                    if el.nargs == "+" and el.value == el.default:
                        raise RequiredArgumentError(el)
                results[el.dest] = el.value

        return SimpleNamespace(**results)
