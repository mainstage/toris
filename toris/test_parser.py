import pytest, shlex

from .errors import (
    ParserError,
    InvalidChoiceError,
    ValueNeededError,
    RequiredArgumentError,
    NotEnoughArgumentsError,
)

from .parser import Parser
from .arguments import Argument, Option, Flag
from .parsable import CommandChoice


@pytest.fixture
def basic_options():
    return [
        Option("--basic1"),
        Option("--basic2", "-b"),
    ]


@pytest.fixture
def basic_options_w_command(basic_options):
    return basic_options + [CommandChoice("cmd1", "cmd2")]


@pytest.fixture
def basic_options_w_flags(basic_options):
    return basic_options + [
        Flag("--flag", "-f"),
        Flag("--other", "-o"),
    ]


def test_basic_parse_next(basic_options):
    argv1 = shlex.split("lolilol")

    parser1 = Parser(argv1, basic_options)
    assert parser1.parse_next() is None

    samples = [
        "--basic1 v1",
        "--basic1=v1",
        "-b v1",
    ]
    for arg in samples:
        arg = shlex.split(arg)

        print(arg)
        parser = Parser(arg, basic_options)
        el = parser.parse_next()
        assert el is not None
        assert el.value == "v1"


def test_flag_parse_next(basic_options_w_flags):
    samples = [
        "--flag --other",
        "--flag -o" "-fo",
    ]
    for arg in samples:
        arg = shlex.split(arg)

        print(arg)
        parser = Parser(arg, basic_options_w_flags)
        el = parser.parse_next()
        assert el is not None
        assert el.metavar == "flag"
        el = parser.parse_next()
        assert el is not None
        assert el.metavar == "other"


def test_basic_parse_next_cmd(basic_options_w_command):
    argv1 = shlex.split("--basic1 v1 cmd1")
    parser1 = Parser(argv1, basic_options_w_command)

    el1 = parser1.parse_next()
    assert el1 is not None
    assert len(parser1.argv) == 1
    el2 = parser1.parse_next()
    assert el2 is not None
    assert el2.value == "cmd1"
    assert len(parser1.parsables) == 2
    assert len(parser1.parsed) == 2


def test_basic_parse(basic_options):
    argv = shlex.split("--basic1 v1 -b v2")
    parser = Parser(argv, basic_options)

    res = parser.parse()
    assert res is not None
    assert len(parser.parsed) == 2


def test_basic_parse_error(basic_options):
    argv = shlex.split("--basic1 v1 --fail v2")
    parser = Parser(argv, basic_options)

    with pytest.raises(ParserError) as exc:
        res = parser.parse()


def test_parser_options_actions():
    parsables = [
        Option("--basic"),
        Option("--basic-int", type=int),
        Option("--store-const", action="store_const", const="const"),
        Option("--append", action="append"),
        Option("--append-const", action="append_const", const="const"),
        Option("--store-true", action="store_true"),
        Option("--store-false", action="store_false"),
        Option("--choice", choices=["a", "b"]),
        Option("--dest", dest="my_dest"),
    ]
    argv = "--basic basic --basic-int 42 --store-const x "
    argv += "--append a --append b --append-const a --append-const b "
    argv += "--store-true false --store-false true --choice a --dest test"

    parser = Parser(shlex.split(argv), parsables)
    parser.parse()
    res = parser.results

    assert res.basic == "basic"
    assert res.basic_int == 42
    assert res.store_const == "const"
    assert res.append == ["a", "b"]
    assert res.store_true == True
    assert res.store_false == False
    assert res.choice == "a"
    assert res.my_dest == "test"


def test_parser_options_errors():
    arg1 = [Option("--required", required=True)]
    parser1 = Parser([], arg1)
    with pytest.raises(RequiredArgumentError) as exc:
        parser1.parse()
        parser1.results()

    args_choice = [Option("-c", choices=["a", "b"])]
    parser_choice = Parser(["-c", "z"], args_choice)
    with pytest.raises(InvalidChoiceError) as exc:
        parser_choice.parse()

    args_needed = [Option("--basic", "-b")]
    parser_needed = Parser(["--basic"], args_needed)
    with pytest.raises(ValueNeededError) as exc:
        parser_needed.parse()

    parser_needed2 = Parser(["-b"], args_needed)
    with pytest.raises(ValueNeededError) as exc:
        parser_needed2.parse()


def test_parser_double_terminal():
    with pytest.raises(ParserError) as exc:
        parser = Parser([], [CommandChoice("cmd1"), CommandChoice("cmd2")])


def test_parser_arguments_basic():
    parser_plus = Parser(
        shlex.split("one plus plus plus"),
        [Argument("one", nargs="1"), Argument("plus", nargs="+")],
    )
    assert parser_plus.parse()
    res = parser_plus.results
    assert res.one == "one"
    assert res.plus == ["plus", "plus", "plus"]

    parser_star = Parser(
        shlex.split("? star star"),
        [Argument("question", nargs="?"), Argument("star", nargs="*")],
    )
    assert parser_star.parse()
    res = parser_star.results
    assert res.question == "?"
    assert res.star == ["star", "star"]


def test_parser_complete():
    parser = Parser(
        shlex.split("-vv -n name question -n name --verbose plus plus"),
        [
            Flag("--verbose", "-v", action="count"),
            Option("--name", "-n", action="append"),
            Argument("question", nargs="?"),
            Argument("plus", nargs="+"),
        ],
    )
    parser.parse()
    res = parser.results

    assert res.verbose == 3
    assert res.name == ["name", "name"]
    assert res.question == "question"
    assert res.plus == ["plus", "plus"]


def test_parser_invalid_opt_name():
    with pytest.raises(ValueError) as exc:
        Option("invalid")


def test_parser_invalid_nargs():
    with pytest.raises(ValueError) as exc:
        Argument("invalid", nargs="nope")


def test_parser_nargs_not_enough():
    with pytest.raises(NotEnoughArgumentsError) as exc:
        Parser(shlex.split("1 2"), [Argument("three", nargs=3)]).parse()


def test_parser_nargs_plus_required():
    with pytest.raises(RequiredArgumentError) as exc:
        parser = Parser(shlex.split(""), [Argument("plus", nargs="+")])
        parser.parse()
        parser.results


def test_parser_invalid_action():
    with pytest.raises(ValueError) as exc:
        Argument("invalid", action="nope")
