import abc

from typing import Optional, List, Callable, Any

from .errors import InvalidChoiceError


class Parsable:
    """Represent a parsable element. The naming sucks, I know.

    An parsable is something that can be parsed in the list of arguments from
    the command line (usually sys.argv), like an option, a positional argument
    or the name of a command.

    The parsing of each Parsables done in the Parsable itself, via the `accept`
    method, which is called repeatedly by the parser. The accept method must
    return True and consume some input in `parser.argv` when it recognizes
    the next token(s) (`parser.top`) as his.
    """

    parser_prepend: bool = False
    parser_remove: bool = False
    parser_terminal: bool = False

    VALID_ACTIONS: List[str] = [
        "store",
        "store_const",
        "store_true",
        "store_false",
        "append",
        "append_const",
        "count",
    ]

    def __init__(
        self,
        metavar: str,
        action: str = "store",
        choices: Optional[List[Any]] = None,
        const: Any = None,
        default: Any = None,
        dest: Optional[str] = None,
        deprecated: bool = False,
        group: Optional[str] = None,
        help: Optional[str] = None,
        required: bool = False,
        type: Callable = None,
    ):
        "base class for Parsable stuff"

        self.choices = choices
        self.const = const
        self.deprecated = deprecated
        self.dest = dest or metavar
        self.group = group
        self.help = help
        self.metavar = metavar
        self.required = required
        self.type = type

        valid_actions = self.__class__.VALID_ACTIONS
        if action not in valid_actions:
            msg = f"Action '{action}' invalid. Allowed values: {valid_actions}"
            raise ValueError(msg)
        self.action = action

        if default is None and action in ("append", "append_const"):
            default = []
        if default is None and action == "count":
            default = 0

        self.default = default

        if hasattr(self.default, "copy"):
            self._value = default.copy()
        else:
            self._value = default

    @property
    def display_name(self):
        return self.metavar

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        # Cast value type using provided function
        if self.type is not None:
            value = self.type(value)

        # Assert valid choice if `choices` was provided
        if self.choices is not None and value not in self.choices:
            raise InvalidChoiceError(self, self.choices, value)

        action = getattr(self, f"action_{self.action}")
        action(value)

    def action_store(self, value):
        self._value = value

    def action_store_const(self, _):
        self._value = self.const

    def action_store_true(self, _):
        self._value = True

    def action_store_false(self, _):
        self._value = False

    def action_append(self, value):
        self._value.append(value)

    def action_append_const(self, _):
        self._value.append(self.const)

    def action_count(self, _):
        print("COUNT", self._value)
        self._value += 1

    @abc.abstractmethod
    def accept(self, parser) -> bool:
        pass  # pragma: no cover

    def __rich_repr__(self):
        yield "metavar", self.metavar
        for name in ["default", "choices", "type", "required", "const"]:
            attr = getattr(self, name)
            if attr is not None:
                yield name, attr


class CommandChoice(Parsable):
    """
    An internal Parsable that is used to represent the need to choose a
    Command to execute
    """

    parser_remove = True
    parser_terminal = True

    def __init__(self, *names):
        super().__init__(metavar="_cmd_")
        self.names = names

    def accept(self, parser):
        if parser.top in self.names:
            self.value = parser.pop()
            return True

    def __rich_repr__(self):
        yield "names", self.names
