import inspect

from rich import print

from .errors import HelpRequestedError
from .parser import CommandChoice
from .arguments import Flag, Argument

HELP_FLAGS = ("--help", "-h")
HELP_HELP = "Show this message and exit"


class HelpFlag(Flag):
    "The '--help' flag expected in all CLI app"

    def __init__(self):
        super().__init__(*HELP_FLAGS, metavar="_help_", help=HELP_HELP)

    def consume_long(self, _parser, _name):
        raise HelpRequestedError()

    def consume_short(self, _parser, _name):
        raise HelpRequestedError()


class HelpPrinter:
    @classmethod
    def help(cls, group, parser, cmd=None):
        cls(group, parser, cmd).print_help()

    def __init__(self, group, parser, cmd=None):
        self.group = group
        self.main = group.main
        self.parser = parser
        self.cmd = cmd

        formatter_class = self.main.help_formatter
        self.formatter = formatter_class(self.group, console=self.main.stderr)

    def print_help(self):
        self.formatter.print_prolog()
        self.print_usage()
        self.print_arguments()
        self.print_options()
        self.formatter.print_epilog()

    def get_main_help(self):
        if self.cmd:
            return self.cmd.__toris_meta__["help"]
        else:
            return self.group._group_help

    def get_current_positionals(self):
        "Already parsed positionals"
        result = []

        for item in self.parser.parsed:
            if isinstance(item, CommandChoice) or isinstance(item, Argument):
                result.append(item)

        return result

    def get_future_positionals(self):
        "Positionals yet to be parsed"
        result = []

        for item in self.parser.parsables:
            if isinstance(item, CommandChoice) or isinstance(item, Argument):
                result.append(item)

        return result

    def print_usage(self):
        main_name = self.main.label
        current_pos = self.get_current_positionals()
        future_pos = self.get_future_positionals()
        main_help = self.get_main_help()

        self.formatter.print_usage(
            main_name, current_pos, future_pos, main_help, self.cmd is not None
        )

        if not self.cmd:
            groups = self.group.subgroups
            cmds = self.group._collect_commands()
            metas = [cmd.__toris_meta__ for cmd in cmds]

            self.formatter.print_subcommands_list(groups, metas)

    def print_arguments(self):
        arguments = self.parser.parsed_arguments + self.parser.arguments

        if len(arguments) > 0:
            self.formatter.print_arguments(arguments)

    def print_options(self):
        options = self.parser.options

        if len(options) > 0:
            self.formatter.print_options(reversed(options))
