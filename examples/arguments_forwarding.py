from toris import Cli, command, argument

#
# Run me with :
# - poetry run python examples/arguments_forwarding.py -- forward -- --help
# (The first `--` is consumed by peotry)
#

class ArgumentsForwarding(Cli):
    """
    An example with forwarding arguments after `--`
    """

    @command(
        help="Fetch un-processed arguments after `--`",
        parse=[
            argument('forwarded', nargs="*")
        ]
    )
    def forward(self):
        self.print("Try to run me with '-- --help'")
        self.print(self.args)


if __name__ == "__main__":
    ArgumentsForwarding.run()
