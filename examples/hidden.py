from toris import Cli, command

#
# Run me with :
# - `poetry install`
# - `poetry run python examples/simple.py hidden`
#


class Hidden(Cli):
    """
    A simple toris CLI
    """

    @command(hide=True)
    def hidden(self):
        self.stdout.print("This command is [dim]hidden[/]!")


if __name__ == "__main__":
    Hidden.run()
