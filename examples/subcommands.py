from toris import Cli, Group, command, argument

#
# Run me with :
# - poetry run python examples/subcommands.py
#


class ProjectCommands(Group):
    """
    Project management commands
    """

    label = "project"

    @command(
        summary="Creates a new project",
        parse=[argument("name", help="The project name")],
    )
    def create(self):
        self.print("Creating project {self.args.name}")

    @command(
        summary="Deletes a project",
        parse=[argument("name", help="The project name")],
    )
    def delete(self):
        self.print("Deleting project {self.args.name}")

    @command(summary="List the projects")
    def list(self):
        for name in ["project-a", "project-b", "super-project"]:
            self.print(f"  - {name}")


class ConfigCommands(Group):
    """
    Configure the CLI tool
    """

    label = "config"

    @command(summary="Dump current settings")
    def dump(self):
        # Using random data to have something to print
        self.print(self.__dict__)

    @command(
        summary="Set a config value",
        parse=[
            argument(
                "name",
                help="""
            The name of the setting to set, or anything really as this is just as
            dummy example that does nothing
            """,
            ),
            argument("value", help="The setting new value"),
        ],
    )
    def set(self):
        # Using random data to have something to print
        self.print("Setting {self.args.name} to {self.args.value}")


class SubcommandsExample(Cli):
    """
    A simple toris CLI
    """

    subgroups = [ProjectCommands, ConfigCommands]


if __name__ == "__main__":
    SubcommandsExample.run()
