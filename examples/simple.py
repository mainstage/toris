from toris import Cli, command

#
# Run me with :
# - `poetry install`
# - `poetry run python examples/simple.py`
# - `poetry run python examples/simple.py simple`
#


class Simple(Cli):
    """
    A simple toris CLI
    """

    @command()
    def simple(self):
        #
        # self.stdout and self.stderr are rich.Console objects, which
        # means they're capable of all sorts of fancy stuff:
        #
        # See https://rich.readthedocs.io/en/stable/console.html
        #
        self.stdout.print("Hello [bold]World[/bold] !")


if __name__ == "__main__":
    Simple.run()
