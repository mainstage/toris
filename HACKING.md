# Hacking

Various notes about contributing to and releasing toris

## Releasing

Release a new version is done using Commitizen (i.e. `cz`):

- cz bump --changelog # --prerelease alpha
- git push origin main --tags
